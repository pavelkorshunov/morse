<?php

class MorseDecoder
{
    /**
     * Переводит из морзе в человекопонятную строку
     *
     * @param string $code
     * @return mixed
     */
    public static function decodeMorseToSymbol($code)
    {
        if(is_string($code)) {
            $words = explode("   ", trim($code));
            $sentence = "";

            foreach ($words as $word) {
                $symbol = explode(" ", $word);
                $sentence .= self::morseWordDecode($symbol) . " ";
            }

            return trim($sentence);
        }

        return false;
    }

    /**
     * Возвращает слово по коду Морзе
     *
     * @param array $morseWordArr
     * @return mixed
     */
    public static function morseWordDecode($morseWordArr)
    {
        if(is_array($morseWordArr)) {
            $word = "";
            foreach ($morseWordArr as $symbol) {
                $word .= self::morseSymbolDecode($symbol);
            }

            return $word;
        }

        return false;
    }

    /**
     * Возвращает букву по коду Морзе
     *
     * @param string $symbolOfMorse
     * @return mixed
     */
    public static function morseSymbolDecode($symbolOfMorse)
    {
        if(is_string($symbolOfMorse)) {
            return array_search($symbolOfMorse, MORSE);
        }

        return false;
    }
}