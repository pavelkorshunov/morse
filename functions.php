<?php

if(!function_exists("decode_morse_to_symbol")) {

    /**
     * Переводит из морзе в человекопонятную строку
     *
     * @param string $code
     * @return mixed
     */
    function decode_morse_to_symbol($code)
    {
        if(is_string($code)) {
            $words = explode("   ", trim($code));
            $sentence = "";

            foreach ($words as $word) {
                $symbol = explode(" ", $word);
                $sentence .= morse_word_decode($symbol) . " ";
            }

            return trim($sentence);
        }

        return false;
    }
}

if(!function_exists("morse_word_decode")) {

    /**
     * Возвращает слово по коду Морзе
     *
     * @param array $morseWordArr
     * @return mixed
     */
    function morse_word_decode($morseWordArr)
    {
        if(is_array($morseWordArr)) {
            $word = "";
            foreach ($morseWordArr as $symbol) {
                $word .= morse_symbol_decode($symbol);
            }

            return $word;
        }

        return false;
    }
}

if(!function_exists("morse_symbol_decode")) {

    /**
     * Возвращает букву по коду Морзе
     *
     * @param string $symbolOfMorse
     * @return mixed
     */
    function morse_symbol_decode($symbolOfMorse)
    {
        if(is_string($symbolOfMorse)) {
            return array_search($symbolOfMorse, MORSE);
        }

        return false;
    }
}