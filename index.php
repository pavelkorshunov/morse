<?php
require_once "table.php";
require_once "functions.php";
require_once "MorseDecoder.php";

// Функция
echo decode_morse_to_symbol("-.-- --- ..-   .- -. -..   -- . .-.-.-   -. .- -- .   .-..-. .-- --- --- -.. .-..-.");
echo "<br>";
echo decode_morse_to_symbol("...---...");
echo "<br>";

// Метод класса
echo MorseDecoder::decodeMorseToSymbol("-.-- --- ..-   .- -. -..   -- . .-.-.-   -. .- -- .   .----. .-- --- --- -..   -.... .----.");